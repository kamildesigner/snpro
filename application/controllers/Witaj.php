<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Witaj extends CI_Controller {

	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function dokumentacja(){
		$this->load->view('agencjazyskow/dokumentacja');
	}

}
